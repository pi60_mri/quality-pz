﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5.PageObjects
{
    public class VetPageObject : BasePageObject
    {
        public VetPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By VetAdd = By.CssSelector(".addVet");
        private By VetSave = By.CssSelector(".saveVet");
        private By VetLast = By.CssSelector("tr:last-of-type > td");
        private By VetEdit = By.CssSelector("tr:last-of-type .editVet");
        private By VetUpdate = By.CssSelector(".updateVet");
        private By VetFirstName = By.Id("firstName");
        private By VetLastName = By.Id("lastName");

        public void EnterVetData()
        {
            Helper.ClickAndSendKeys(driver.FindElement(VetFirstName), "Роман");
            Helper.ClickAndSendKeys(driver.FindElement(VetLastName), "Мельник");
        }

        public void EnterVetNewName()
        {
            Helper.ClickAndClearAndSendKeys(driver.FindElement(VetFirstName), "Василь");
        }

        public void ClickAddVet()
        {
            driver.FindElement(VetAdd).Click();
        }

        public void ClickSaveVet()
        {
            driver.FindElement(VetSave).Click();
        }

        public string GetVetLastName()
        {
            return driver.FindElement(VetLast).Text;
        }

        public void ClickVetEdit()
        {
            driver.FindElement(VetEdit).Click();
        }

        public void ClickVetUpdate()
        {
            driver.FindElement(VetUpdate).Click();
        }
    }
}
