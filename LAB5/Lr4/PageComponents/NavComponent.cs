﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5.PageComponents
{
    public class NavComponent
    {
        private IWebDriver driver;

        public NavComponent(IWebDriver driver) => this.driver = driver;

        private IWebElement NavHome() => driver.FindElement(By.CssSelector(".nav > li:nth-child(1) > a"));
        private IWebElement NavLogo() => driver.FindElement(By.CssSelector(".navbar - brand > span"));
        private IWebElement NavOwner() => driver.FindElement(By.CssSelector(".ownerTab"));
        private IWebElement NavOwnerAdd() => driver.FindElement(By.CssSelector(".open li:nth-child(2) > a"));
        private IWebElement NavOwnerAll() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));
        private IWebElement NavPetType() => driver.FindElement(By.CssSelector("li:nth-child(4) > a"));
        private IWebElement NavSpecialty() => driver.FindElement(By.CssSelector("li:nth-child(5) span:nth-child(2)"));
        private IWebElement NavVet() => driver.FindElement(By.CssSelector(".vetsTab"));
        private IWebElement NavVetAdd() => driver.FindElement(By.CssSelector(".open li:nth-child(2) span:nth-child(2)"));
        private IWebElement NavVetAll() => driver.FindElement(By.CssSelector(".open li:nth-child(1) > a"));

        public void ClickHome()
        {
            NavHome().Click();
        }

        public void ClickLogo()
        {
            NavLogo().Click();
        }

        public void ClickOwnerTab()
        {
            NavOwner().Click();
        }

        public void ClickOwnerAdd()
        {
            NavOwnerAdd().Click();
        }

        public void ClickOwnerAll()
        {
            NavOwnerAll().Click();
        }

        public void ClickPetTypeTab()
        {
            NavPetType().Click();
        }

        public void ClickSpecialtyTab()
        {
            NavSpecialty().Click();
        }

        public void ClickVetTab()
        {
            NavVet().Click();
        }

        public void ClickVetAdd()
        {
            NavVetAdd().Click();
        }

        public void ClickVetAll()
        {
            NavVetAll().Click();
        }
    }
}
