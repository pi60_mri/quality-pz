﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        public HomePageObject(IWebDriver driver) : base(driver) 
        {
        
        }

        private By HomeHeader = By.CssSelector("h1");

        public string GetHomeHeader()
        {
            return driver.FindElement(HomeHeader).Text;
        }
    }
}
