﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr4.PageObjects
{
    public class VetPageObject : BasePageObject
    {
        public VetPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By VetTab = By.CssSelector(".vetsTab");
        private By VetOpenAdd = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By VetOpenAll = By.CssSelector(".open li:nth-child(1) > a");
        private By VetAdd = By.CssSelector(".addVet");
        private By VetSave = By.CssSelector(".saveVet");
        private By VetLast = By.CssSelector("tr:last-of-type > td");
        private By VetEdit = By.CssSelector("tr:last-of-type .editVet");
        private By VetUpdate = By.CssSelector(".updateVet");
        private By VetFirstName = By.Id("firstName");
        private By VetLastName = By.Id("lastName");

        public void ClickVetTab()
        {
            driver.FindElement(VetTab).Click();
        }

        public void ClickVetAdd()
        {
            driver.FindElement(VetOpenAdd).Click();
        }

        public void ClickVetAll()
        {
            driver.FindElement(VetOpenAll).Click();
        }

        public void EnterVetData()
        {
            driver.FindElement(VetFirstName).Click();
            driver.FindElement(VetFirstName).SendKeys("Роман");
            driver.FindElement(VetLastName).Click();
            driver.FindElement(VetLastName).SendKeys("Мельник");
        }

        public void EnterVetNewName()
        {
            driver.FindElement(VetFirstName).Click();
            driver.FindElement(VetFirstName).Clear();
            driver.FindElement(VetFirstName).SendKeys("Василь");
        }

        public void ClickAddVet()
        {
            driver.FindElement(VetAdd).Click();
        }

        public void ClickSaveVet()
        {
            driver.FindElement(VetSave).Click();
        }

        public string GetVetLastName()
        {
            return driver.FindElement(VetLast).Text;
        }

        public void ClickVetEdit()
        {
            driver.FindElement(VetEdit).Click();
        }

        public void ClickVetUpdate()
        {
            driver.FindElement(VetUpdate).Click();
        }
    }
}
