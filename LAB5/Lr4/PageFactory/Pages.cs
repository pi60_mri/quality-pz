﻿using Lr5.PageObjects;

namespace Lr5.PageFactory
{
    public static class Pages
    {
        public static HomePageObject Home => new HomePageObject(BaseTest.driver);
        public static OwnerPageObject Owner => new OwnerPageObject(BaseTest.driver);
        public static PetTypePageObject PetType => new PetTypePageObject(BaseTest.driver);
        public static SpecialtyPageObject Specialty => new SpecialtyPageObject(BaseTest.driver);
        public static VetPageObject Vet => new VetPageObject(BaseTest.driver);
    }
}
