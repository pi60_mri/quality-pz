﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Lr4
{
    public abstract class BaseTest
    {
        protected IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://client.sana-commerce.dev");
            driver.Manage().Window.Size = new System.Drawing.Size(1076, 678);
        }

        [TearDown]
        protected void TearDown()
        {
            driver.Quit();
        }

        public void Wait()
        {
            Thread.Sleep(2000);
        }
    }
}
