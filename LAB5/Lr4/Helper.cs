﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5
{
    class Helper
    {
        public static void ClickAndSendKeys(IWebElement webElement, string value)
        {
            webElement.Click();
            webElement.SendKeys(value);
        }

        public static void ClickAndClearAndSendKeys(IWebElement webElement, string value)
        {
            webElement.Click();
            webElement.Clear();
            webElement.SendKeys(value);
        }

        public static void Wait()
        {
            Thread.Sleep(2000);
        }
    }
}
