// Generated by Selenium IDE
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class ClickLogoTest : BaseTest
    {
        [Test]
        public void ClickLogo()
        {
            NavComponent nav = Components.Nav;
            HomePageObject home = Pages.Home;
            nav.ClickLogo();
            Assert.True(home.GetHomeHeader() == "Welcome to Petclinic");
        }
    }
}