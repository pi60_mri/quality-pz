﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr4.PageObjects
{
    public class PetTypePageObject : BasePageObject
    {
        public PetTypePageObject(IWebDriver driver) : base(driver)
        {

        }

        private By PetTypeTab = By.CssSelector("li:nth-child(4) > a");
        private By PetTypeName = By.Id("name");
        private By PetTypeAdd = By.CssSelector(".addPet");
        private By PetTypeSave = By.CssSelector(".saveType");
        private By PetTypeLast = By.CssSelector("tr:last-of-type > td > input");
        private By PetTypeEdit = By.CssSelector("tr:last-of-type .editPet");
        private By PetTypeUpdate = By.CssSelector(".updatePetType");

        public void ClickPetTypeTab()
        {
            driver.FindElement(PetTypeTab).Click();
        }

        public void EnterPetTypeData()
        {
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).SendKeys("Кінь");
        }

        public void EnterPetTypeNewName()
        {
            driver.FindElement(PetTypeName).Click();
            driver.FindElement(PetTypeName).Clear();
            driver.FindElement(PetTypeName).SendKeys("Кінь2");
        }

        public void ClickAddPetType()
        {
            driver.FindElement(PetTypeAdd).Click();
        }

        public void ClickSavePetType()
        {
            driver.FindElement(PetTypeSave).Click();
        }

        public string GetPetTypeLastName()
        {
            return driver.FindElement(PetTypeLast).GetAttribute("value");
        }

        public void ClickPetTypeEdit()
        {
            driver.FindElement(PetTypeEdit).Click();
        }

        public void ClickPetTypeUpdate()
        {
            driver.FindElement(PetTypeUpdate).Click();
        }
    }
}
