﻿using Lr5.PageComponents;

namespace Lr5.PageFactory
{
    public static class Components
    {
        public static NavComponent Nav => new NavComponent(BaseTest.driver);
    }
}
