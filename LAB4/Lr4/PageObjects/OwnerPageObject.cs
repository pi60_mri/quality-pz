﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr4.PageObjects
{
    public class OwnerPageObject : BasePageObject
    {
        public OwnerPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By OwnerTab = By.CssSelector(".ownerTab");
        private By OwnerOpenAdd = By.CssSelector(".open li:nth-child(2) > a");
        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerTelephone = By.Id("telephone");
        private By OwnerAdd = By.CssSelector(".addOwner");
        private By OwnerOpenAll = By.CssSelector(".open li:nth-child(1) > a");
        private By OwnerLast = By.CssSelector("tr.petOwner:last-of-type > td:first-of-type > a");
        private By OwnerEdit = By.CssSelector(".editOwner");
        private By OwnerUpdate = By.CssSelector(".updateOwner");

        public void ClickOwnerTab()
        {
            driver.FindElement(OwnerTab).Click();
        }

        public void ClickOwnerAdd()
        {
            driver.FindElement(OwnerOpenAdd).Click();
        }

        public void ClickOwnerAll()
        {
            driver.FindElement(OwnerOpenAll).Click();
        }

        public void EnterOwnerData()
        {
            driver.FindElement(OwnerFirstName).Click();
            driver.FindElement(OwnerFirstName).SendKeys("Роман");
            driver.FindElement(OwnerLastName).Click();
            driver.FindElement(OwnerLastName).SendKeys("Мельник");
            driver.FindElement(OwnerAddress).Click();
            driver.FindElement(OwnerAddress).SendKeys("Чуднівська, 103");
            driver.FindElement(OwnerCity).Click();
            driver.FindElement(OwnerCity).SendKeys("Житомир");
            driver.FindElement(OwnerTelephone).Click();
            driver.FindElement(OwnerTelephone).SendKeys("0981234567");
        }

        public void EnterOwnerNewName()
        {
            driver.FindElement(OwnerFirstName).Click();
            driver.FindElement(OwnerFirstName).Clear();
            driver.FindElement(OwnerFirstName).SendKeys("Василь"); 
        }

        public void ClickAddOwner()
        {
            driver.FindElement(OwnerAdd).Click();
        }

        public string GetOwnerLastName()
        {
            return driver.FindElement(OwnerLast).Text;
        }

        public void ClickOwnerEdit()
        {
            driver.FindElement(OwnerEdit).Click();
        }

        public void ClickOwnerUpdate()
        {
            driver.FindElement(OwnerUpdate).Click();
        }

        public void ClickOwner()
        {
            driver.FindElement(OwnerLast).Click();
        }
    }
}
