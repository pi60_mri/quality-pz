﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr4.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        public HomePageObject(IWebDriver driver) : base(driver) 
        {
        
        }

        private By HomeNav = By.CssSelector(".nav > li:nth-child(1) > a");
        private By HomeHeader = By.CssSelector("h1");
        private By HomeLogo = By.CssSelector(".navbar - brand > span");

        public void ClickHome()
        {
            driver.FindElement(HomeNav).Click();
        }

        public string GetHomeHeader()
        {
            return driver.FindElement(HomeHeader).Text;
        }

        public void ClickLogo()
        {
            driver.FindElement(HomeLogo).Click();
        }
    }
}
