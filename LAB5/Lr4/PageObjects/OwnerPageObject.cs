﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5.PageObjects
{
    public class OwnerPageObject : BasePageObject
    {
        public OwnerPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By OwnerFirstName = By.Id("firstName");
        private By OwnerLastName = By.Id("lastName");
        private By OwnerAddress = By.Id("address");
        private By OwnerCity = By.Id("city");
        private By OwnerTelephone = By.Id("telephone");
        private By OwnerAdd = By.CssSelector(".addOwner");
        private By OwnerLast = By.CssSelector("tr.petOwner:last-of-type > td:first-of-type > a");
        private By OwnerEdit = By.CssSelector(".editOwner");
        private By OwnerUpdate = By.CssSelector(".updateOwner");

        public void EnterOwnerData()
        {
            Helper.ClickAndSendKeys(driver.FindElement(OwnerFirstName), "Роман");
            Helper.ClickAndSendKeys(driver.FindElement(OwnerLastName), "Мельник");
            Helper.ClickAndSendKeys(driver.FindElement(OwnerAddress), "Чуднівська, 103");
            Helper.ClickAndSendKeys(driver.FindElement(OwnerCity), "Житомир");
            Helper.ClickAndSendKeys(driver.FindElement(OwnerTelephone), "0981234567");
        }

        public void EnterOwnerNewName()
        {
            Helper.ClickAndClearAndSendKeys(driver.FindElement(OwnerFirstName), "Василь");
        }

        public void ClickAddOwner()
        {
            driver.FindElement(OwnerAdd).Click();
        }

        public string GetOwnerLastName()
        {
            return driver.FindElement(OwnerLast).Text;
        }

        public void ClickOwnerEdit()
        {
            driver.FindElement(OwnerEdit).Click();
        }

        public void ClickOwnerUpdate()
        {
            driver.FindElement(OwnerUpdate).Click();
        }

        public void ClickOwner()
        {
            driver.FindElement(OwnerLast).Click();
        }
    }
}
