﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr5.PageObjects
{
    public class SpecialtyPageObject : BasePageObject
    {
        public SpecialtyPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By SpecialtyName = By.Id("name");
        private By SpecialtyAdd = By.CssSelector(".addSpecialty");
        private By SpecialtySave = By.CssSelector(".btn:nth-child(3)");
        private By SpecialtyLast = By.CssSelector("tr:last-of-type > td > input");
        private By SpecialtyEdit = By.CssSelector("tr:last-of-type .editSpecialty");
        private By SpecialtyUpdate = By.CssSelector(".updateSpecialty");

        public void EnterSpecialtyData()
        {
            Helper.ClickAndSendKeys(driver.FindElement(SpecialtyName), "Гінеколог");
        }

        public void EnterSpecialtyNewName()
        {
            Helper.ClickAndClearAndSendKeys(driver.FindElement(SpecialtyName), "Гінеколог2");
        }

        public void ClickAddSpecialty()
        {
            driver.FindElement(SpecialtyAdd).Click();
        }

        public void ClickSaveSpecialty()
        {
            driver.FindElement(SpecialtySave).Click();
        }

        public string GetSpecialtyLastName()
        {
            return driver.FindElement(SpecialtyLast).GetAttribute("value");
        }

        public void ClickSpecialtyEdit()
        {
            driver.FindElement(SpecialtyEdit).Click();
        }

        public void ClickSpecialtyUpdate()
        {
            driver.FindElement(SpecialtyUpdate).Click();
        }
    }
}
